# wget "https://get.fontspace.co/download/family/8j4z/36725246f64c4594a48441c88f803e0b/liberation-sans-font.zip"
curl -L -o liberation-sans-font.zip "https://get.fontspace.co/download/family/8j4z/36725246f64c4594a48441c88f803e0b/liberation-sans-font.zip"
# "/c/Program Files/7-Zip/7z.exe" e liberation-sans-font.zip LiberationSansBold-1adM.ttf LiberationSansItalic-RJre.ttf LiberationSans-BaDn.ttf
unzip liberation-sans-font.zip LiberationSansBold-1adM.ttf LiberationSansItalic-RJre.ttf LiberationSans-BaDn.ttf
mv LiberationSansBold-1adM.ttf LiberationSans-Bold.ttf
mv LiberationSansItalic-RJre.ttf LiberationSans-Italic.ttf
mv LiberationSans-BaDn.ttf LiberationSans-Regular.ttf
