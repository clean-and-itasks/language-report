# Language report

This is the repository of the Clean language report. The report documents the syntax and features
of the [Clean programming language](https://clean-lang.org).

This is a delayed mirror of the [upstream](https://gitlab.science.ru.nl/clean-compiler-and-rts/language-report) version
and is only used to publish the package. Periodically, changes from upstream are released in a new version here.
