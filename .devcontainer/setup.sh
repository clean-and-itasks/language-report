apt-get update -qq
apt-get install -yy --no-install-recommends curl unzip git ca-certificates
mkdir -p bin
cd bin
curl -L -o liberation-sans-font.zip https://get.fontspace.co/download/family/8j4z/36725246f64c4594a48441c88f803e0b/liberation-sans-font.zip
unzip liberation-sans-font.zip LiberationSansBold-1adM.ttf LiberationSansItalic-RJre.ttf LiberationSans-BaDn.ttf
mv LiberationSansBold-1adM.ttf LiberationSans-Bold.ttf
mv LiberationSansItalic-RJre.ttf LiberationSans-Italic.ttf
mv LiberationSans-BaDn.ttf LiberationSans-Regular.ttf
git clone --depth=1 git://git.ghostscript.com/urw-core35-fonts.git
cp urw-core35-fonts/NimbusMonoPS-Regular.ttf .
cp urw-core35-fonts/NimbusMonoPS-Bold.ttf .
